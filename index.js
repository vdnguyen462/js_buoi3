/* ----------Bài 1 ----------
* Đầu vào: 3 số nguyên
*
* Các bước xử lý:
* B1: Tạo 3 biến nhập số nguyên.
* B2: Tạo 1 biến tạm và gán giá trị bằng 0.
* B3: Xét a>b thì hoán đổi vị trí theo thứ tự tăng dần, tương tự cho trường hợp a>c, và trường hợp b>c.
* B4: In ra kết quả sắp xếp.
*
* Đầu ra: Giá trị sắp xếp theo thứ tự tăng dần.
*/

function sapXep(){
    var a = document.getElementById("txt-so-thu-1").value*1;
    var b = document.getElementById("txt-so-thu-2").value*1;
    var c = document.getElementById("txt-so-thu-3").value*1;
    var temp=0;
    if(a>b){
        temp=a;
        a=b;
        b=temp;
    }
    if(a>c){
        temp=a;
        a=c;
        c=temp;
    }
    if(b>c){
        temp=b;
        b=c;
        c=temp;
    }
    var result1El = document.getElementById("txt_sap_xep").setAttribute('value',`Sắp xếp tăng dần: ${a}, ${b}, ${c}`);
}

/* ----------Bài 2 ----------
* Đầu vào: Ký tự đầu tiên người sử dụng máy.
*
* Các bước xử lý:
* B1: Tạo 1 biến và gán đến id select.
* B2: Xét trường hợp: nếu chọn value B in Bố, M in Mẹ, A in anh Trai, và E in em Gái, nếu không có trong trường hợp trên in kiểm tra lại.
* B3: In ra lời chào.
*
* Đầu ra: Lời chào người sử dụng máy.
*/

function guiLoiChao(){
    var char1 = document.getElementById("select").value;
    if(char1 == "B"){
        var result2El = document.getElementById("txt-gui").setAttribute("value",`Xin chào Bố!`);
    }
    else if(char1 == 'M'){
        var result2El = document.getElementById("txt-gui").setAttribute("value",`Xin chào Mẹ!`);
    }
    else if(char1 == 'A'){
        var result2El = document.getElementById("txt-gui").setAttribute("value",`Xin chào anh Trai!`);
    }
    else if(char1 == 'E'){
        var result2El = document.getElementById("txt-gui").setAttribute("value",`Xin chào em Gái!`);
    }
    else{
        var result2El = document.getElementById("txt-gui").setAttribute("value",`Bạn nhập chưa đúng. Vui lòng thử lại!`);
    }
}

/* ----------Bài 3 ----------
* Đầu vào: 3 số nguyên.
*
* Các bước xử lý:
* B1: Tạo 3 biến nhập tương ứng 3 số nguyên.
* B2: Tạo 2 biến đếm count đếm chẵn,.
* B3: Xét trường hợp: nếu a%2==0 tăng biến đếm chẵn lên 1, ngược lại tăng biến lẻ lên 1, tương tự cho 2 trường hợp b, c.
* B4: In ra kết quả đếm.
*
* Đầu ra: Số lượng chẵn, lẻ.
*/

function demChanLe(){
    var a = document.getElementById("txt-so-1").value*1;
    var b = document.getElementById("txt-so-2").value*1;
    var c = document.getElementById("txt-so-3").value*1;
    var count=0;
    if(a%2==0)
        count++;
    if(b%2==0)
        count++;
    if(c%2==0)
        count++;
    var sole = 3 - count;
    var result3El = document.getElementById("txt-dem").setAttribute('value',`Có ${count} số chẵn, ${sole} số lẻ`);
}

/* ----------Bài 4 ----------
* Đầu vào: Số đo 3 cạnh tam giác.
*
* Các bước xử lý:
* B1: Tạo 3 biến nhập tương ứng 3 cạnh tam giác.
* B2: Xét điều kiện thoả mãn là 1 tam giác a+b>c và b+c>a và c+a>b.
* B3: Xét trường hợp tam giác vuông: nếu a^2+b^2=c^2 hoặc a^2+c^2= b^2 hoặc b^2+c^2=a^2
* B4: Xét trường hợp tam giác cân: nếu a=b hoặc b=c hoặc a=c
* B5: Xét trường hợp tam giác đều: a=b và b=c
* B6: Xét trường hợp tam giác tù: a^2+b^2<c^2 hoặc a^2+c^2< b^2 hoặc b^2+c^2<a^2
* B7: Xét trường hợp còn lại là tam giác nhọn
* B8: In ra kết quả.
*
* Đầu ra: In các loại tam giác.
*/

function kiemTra(){
    var a = document.getElementById("txt-canh-a").value*1;
    var b = document.getElementById("txt-canh-b").value*1;
    var c = document.getElementById("txt-canh-c").value*1;
    if(a+b>c && b+c>a && c+a>b){
        if(a*a+b*b==c*c || a*a+c*c==b*b || b*b+c*c==a*a){
            var result4El = document.getElementById("txt-kiem-tra").setAttribute('value',`Tam giác vuông`);
        }
        else if(a==b && b==c){
            var result4El = document.getElementById("txt-kiem-tra").setAttribute('value',`Tam giác đều`);
        }
        else if(a==b || b==c || a==c){
            var result4El = document.getElementById("txt-kiem-tra").setAttribute('value',`Tam giác cân`);
        }
        else if(a*a+b*b<c*c || a*a+c*c<b*b || b*b+c*c<a*a){
            var result4El = document.getElementById("txt-kiem-tra").setAttribute('value',`Tam giác tù`);
        }
        else{
            var result4El = document.getElementById("txt-kiem-tra").setAttribute('value',`Tam giác nhọn`);
        }
    }
    else{
        var result4El = document.getElementById("txt-kiem-tra").setAttribute('value',`3 số vừa nhập không phải là ba cạnh của tam giác`);
    }
}


function bai1(){
    document.getElementById("e1").style.display = 'block';
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
}

function bai2(){
    document.getElementById("e2").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e3").style.display = "none";
    document.getElementById("e4").style.display = "none";
}

function bai3(){
    document.getElementById("e3").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e4").style.display = "none";
}

function bai4(){
    document.getElementById("e4").style.display = 'block';
    document.getElementById("e1").style.display = "none";
    document.getElementById("e2").style.display = "none";
    document.getElementById("e3").style.display = "none";
}

